#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import re
import sys
import cmd
import json
import inquirer
import subprocess
import configparser
from enum import Enum
from tqdm import tqdm
from tabulate import tabulate
from datetime import datetime

class Arg(Enum):
    ENVIRONMENT = 0
    STAGE       = 1
    CAPABILITY  = 2
    PRINTC      = 3

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

class Terractl(cmd.Cmd):
    intro                   = 'Welcome to the terractl shell.   Type help or ? to list commands.\n'
    default_module_repo     = 'git@gitlab.com:mambucom/product/sre/infrastructure/operations/infrastructure-tf-modules.git'
    live_repo_list          = [
        'git@gitlab.com:mambucom/product/sre/infrastructure/operations/infrastructure-live-production.git',
        'git@gitlab.com:mambucom/product/sre/infrastructure/operations/infrastructure-live-non-prod.git'
    ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._setting_enviroment()

    @property
    def path(self):
        return os.path.join(os.getcwd(), self.live_repo_folder, self.env, self.stage, self.cap)

    @property
    def planfile(self):
        return os.path.join(os.getcwd(), self.live_repo_folder, self.env, self.stage, self.cap, 'plan')

    @property
    def local_module(self):
        return f'{os.getcwd()}/{self.module_repo_folder}//infra/{self.cap}'

    @property
    def terractl_tag_list(self):
        tags = str(subprocess.check_output('git tag', cwd=self.module_repo_folder, shell=True))[1:-1].split('\\n')[:-1]
        
        branches = str(subprocess.check_output('git branch -a', cwd=self.module_repo_folder, shell=True))[4:-2].split('\\n')
        branches = sorted(list(set([banch.split('/')[-1] for banch in branches])))

        return sorted(tags + branches, reverse=True)

    def print_formated_stats(self):
        headers = ['Evironment', 'Capability', 'Plan', 'Apply', 'Locked', 'Error']
        table = []

        for env in self.stats.keys():
            for stage in self.stats[env]:
                for cap in self.stats[env][stage]:
                    info = self.stats[env][stage][cap]
                    changes = info.get('changes', '-')
                    apply = 'Yes' if info.get('apply') else 'No'
                    locked = 'Yes' if info.get('locked') else 'No'
                    error = 'Yes' if info.get('error') else 'No'
                    table.append([env, f'{stage}:{cap}', changes, apply, locked, error])
        
        print(tabulate(table, headers=headers))

    def _action_sign(self, actions, address):
        if len(actions) == 1:
            if "create" in actions:
                return f"{bcolors.OKGREEN}+ {address}{bcolors.ENDC}\n"
            if "update" in actions:
                return f"{bcolors.WARNING}~ {address}{bcolors.ENDC}\n"
            if "delete" in actions:
                return f"{bcolors.FAIL}- {address}{bcolors.ENDC}\n"
            if "read" in actions:
                return f"{bcolors.OKCYAN}<= {address}{bcolors.ENDC}\n"
        elif len(actions) == 2 and "create" in actions and "delete" in actions:
            return f"{bcolors.HEADER}-/+ {address}{bcolors.ENDC}\n"

        return "?"

    def set_environment_var(self, var, message, question=False):
        if not question:
            value = input(message)

            if not value:
                value = getattr(self, 'default_' + var.lower())
                if not value:
                    return self.set_environment_var(message)
        else:
            questions = [
                inquirer.List(var,
                                message=message,
                                choices=getattr(self, var.lower() + '_list'),
                            ),
            ]
            value = inquirer.prompt(questions)[var]
        os.environ[var] = value
        return value

    def _setting_enviroment(self):
        self.stats = {}
        
        self.plan_regex = re.compile('\d+\sto\sadd,\s\d+\sto\schange,\s\d+\sto\sdestroy')

        self.print_commands = ['plan', 'short_plan', 'apply', 'error']

        #check that requiments are met
        self._check_system_requirements()

        # move to tmp folder
        os.chdir('/tmp')

        os.environ['AWS_ASSUME_ROLE_TTL'] = '1h'
        os.environ['AWS_SESSION_TOKEN_TTL'] = '4h'
        
        # set aws profile using .aws/config
        self._set_aws_profile()

        # specify module and live repo to clone and get the folder names of each
        if not os.environ.get('MODULE_REPO'):
            self.set_environment_var('MODULE_REPO', f'Specify module repo (default: {self.default_module_repo}):')
        
        self.module_repo_folder = os.environ.get('MODULE_REPO').split('/')[-1].split('.')[0]

        if not os.environ.get('LIVE_REPO'):
            live_repo = self.set_environment_var('LIVE_REPO', f'Specify live repo:', question=True)
            
        self.live_repo_folder = os.environ.get('LIVE_REPO').split('/')[-1].split('.')[0]

        self._clone_or_pull_repos()

        if not os.environ.get('TERRACTL_TAG'):
            self.set_environment_var('TERRACTL_TAG', 'What infra version you want to deploy?', question=True)
        
        self.tag = os.environ.get('TERRACTL_TAG')

        self.prompt = f'[terractl@{self.tag}] '

        self._setting_autocomplete()

    def _check_system_requirements(self):
        if not subprocess.run(
            ['terraform', '-version'],
            capture_output=True, check=True, text=True
        ).stdout.strip().lstrip('Terraform v').startswith('0.12.'):
            sys.exit('Please use Terraform v0.12')

        if not subprocess.run(
            ['terragrunt', '--version'],
            capture_output=True, check=True, text=True
        ).stdout.strip().lstrip('terragrunt version v').startswith('0.24.'):
            sys.exit('Please use Terragrunt v0.24')    

    def _get_aws_profiles(self):
        config = configparser.ConfigParser()
        config.read(os.path.expanduser('~') + '/.aws/config')

        self.aws_profile_list = sorted(list(set([profile.split(' ')[-1] for profile in config.sections()])))
    
    def trigger_aws_auth(self):
        data = json.loads(subprocess.check_output(['aws-vault', 'exec', os.environ.get('AWS_PROFILE'), '--json', '--prompt=osascript']))

        expiration = datetime.strptime(data.get('Expiration'), '%Y-%m-%dT%H:%M:%SZ')

        remaining_hours = (expiration - datetime.now()).total_seconds()/60/60
        
        if remaining_hours == 0:
            subprocess.check_output(['aws-vault', 'remove', '-s', os.environ.get('AWS_PROFILE')])

        while True:
            try:
                self.aws_vault_cmd = ['aws-vault', 'exec', os.environ['AWS_PROFILE'], '--']
                subprocess.check_output(self.aws_vault_cmd + ['sleep', '0'])
                break
            except subprocess.CalledProcessError as e:
                if 'AWS_VAULT' in str(e.output):
                    sys.exit('Please logout of aws-vault')
                continue

    def _set_aws_profile(self):
        if not os.environ.get('AWS_PROFILE'):
            self._get_aws_profiles()
            
            self.set_environment_var('AWS_PROFILE', 'Select the AWS Profile:', question=True)
        
        self.trigger_aws_auth() 

    def _clone_or_pull_repos(self):
        # clone repos if not found or pull to update
        dir_list = os.listdir('.')

        if self.module_repo_folder not in dir_list:
            print(f'Cloning repo {self.module_repo_folder}')
            subprocess.run(
            ['git', 'clone', f'{os.environ["MODULE_REPO"]}'],
            capture_output=True, check=True
        )
        else:
            print(f'Updating repo {self.module_repo_folder}')
            subprocess.run(
            ['git', 'pull'],
            cwd=self.module_repo_folder, check=True
        )
        
        if self.live_repo_folder not in dir_list:
            print(f'Cloning repo {self.live_repo_folder}')
            subprocess.run(
            ['git', 'clone', f'{os.environ["LIVE_REPO"]}'],
            capture_output=True, check=True
        )
        else:
            print(f'Updating repo {self.live_repo_folder}')
            subprocess.run(
            ['git', 'pull'],
            cwd=self.live_repo_folder, check=True
        )

    def _setting_autocomplete(self):

        self.environments = [f for f in os.listdir(self.live_repo_folder) 
                            if os.path.isdir(os.path.join(self.live_repo_folder, f)) and 
                            not f.startswith('.')]

        self.stages = list(set([stage for env in self.environments for stage in os.listdir(os.path.join(self.live_repo_folder, env)) 
                                    if os.path.isdir(os.path.join(self.live_repo_folder, env, stage)) 
                                    and not stage.startswith('.')]))
        self.capabilities = []
        for env in self.environments:
            for stage in self.stages:
                if os.path.isdir(os.path.join(self.live_repo_folder, env, stage)):
                    for cap in os.listdir(os.path.join(self.live_repo_folder, env, stage)):
                        if os.path.isdir(os.path.join(self.live_repo_folder, env, stage, cap)) and not stage.startswith('.'):
                            self.capabilities.append(cap)
        self.capabilities = list(set(self.capabilities))

        self.autocomplete_lists = {
            Arg.ENVIRONMENT.value: self.environments,
            Arg.STAGE.value: self.stages,
            Arg.CAPABILITY.value: self.capabilities
        }

    def validate_input(self, action, line):
        args = line.split(' ')
        
        if not action == 'print' and not len(args) == 3:
            print('Incorrect number of parameters. Please specify <tag> <envs> <stage> <capabilities>')
            return False
        elif action == 'print' and not len(args) == 4:
            print('Incorrect number of parameters. Please specify <tag> <envs> <stage> <capabilities> <plan|short_plan>')
            return False

        args[Arg.ENVIRONMENT.value] = args[Arg.ENVIRONMENT.value].split(',')
        args[Arg.CAPABILITY.value] = args[Arg.CAPABILITY.value].split(',')

        if args[Arg.STAGE.value] not in self.stages:
            print('Incorrect stage')
            return False
        for cap in args[Arg.CAPABILITY.value]:
            if cap not in self.capabilities:
                print(f'Capability {cap} does not exist')
                return False
        for env in args[Arg.ENVIRONMENT.value]:
            if env not in self.environments:
                print(f'Environment {env} does not exist')
                return False
        for env in args[Arg.ENVIRONMENT.value]:
            for cap in args[Arg.CAPABILITY.value]:
                path = os.path.join(os.getcwd(), self.live_repo_folder, env, args[Arg.STAGE.value], cap)
                if not os.path.isdir(path):
                    print(f'Capability {args[Arg.STAGE.value]}/{cap} for environment {env} does not exist')
                    return False
        if action == 'print' and args[Arg.PRINTC.value] not in self.print_commands:
            print(f'Supported print commands are {self.print_commands}')
            return False
        return args

    def generate_cmd(self, action):
        cmd = ['terragrunt', 'show', '-json', self.planfile]
        if action == 'plan':
            cmd = ['terragrunt', 'plan', '-out', self.planfile, '-compact-warnings']
        elif action == 'apply':
            cmd = ['terragrunt', 'apply', '-input=false', self.planfile]
        elif action == 'destroy':
            cmd = ['terragrunt', 'destroy', '-out', self.planfile, '-compact-warnings']
        elif action == 'force_unlock':
            cmd = ['terragrunt', 'force-unlock', '-force' , self.locked_id]

        return self.aws_vault_cmd + cmd + ['--terragrunt-source', self.local_module]

    def stats_init(self):
        if not self.stats.get(self.env):
            self.stats[self.env] = {}
        if not self.stats[self.env].get(self.stage):
            self.stats[self.env][self.stage] = {}
        if not self.stats[self.env][self.stage].get(self.cap):
            self.stats[self.env][self.stage][self.cap] = {}

    def update_stats(self, action):
        self.stats_init()
        
        if 'Error' in self.output.stderr:
            self.stats[self.env][self.stage][self.cap]['error'] = self.output.stderr
            if "Lock Info:" in self.output.stderr:
                locked_id = re.search('ID+:\s+\w+-\w+-\w+-\w+-\w+', self.output.stderr).group().split()[-1]
                self.stats[self.env][self.stage][self.cap]['locked'] = locked_id
        else:
            if self.stats[self.env][self.stage][self.cap].get('error'):
                del self.stats[self.env][self.stage][self.cap]['error']

        if action == 'show':
            short_plan = f'Short Plan summary for {self.cap}:\n'
            try:
                for res_change in json.loads(self.output.stdout)["resource_changes"]:
                    if "no-op" not in res_change["change"]["actions"]:
                        short_plan += self._action_sign(res_change["change"]["actions"], res_change["address"])
                self.stats[self.env][self.stage][self.cap]['short_plan'] = f"{short_plan} {bcolors.OKGREEN}{self.stats[self.env][self.stage][self.cap]['changes']}{bcolors.ENDC}"
            except:
                print(self.output.stdout)
                print(self.output.stderr)
        elif action == 'plan':
            self.stats[self.env][self.stage][self.cap]['plan'] = self.output.stdout
            self.stats[self.env][self.stage][self.cap]['changes'] = self.plan_regex.search(self.output.stdout).group() if "Plan:" in self.output.stdout else 'No Changes'
        elif action == 'apply':
            if 'Error' not in self.output.stderr:
                self.stats[self.env][self.stage][self.cap]['changes'] = 'No Changes'
            self.stats[self.env][self.stage][self.cap]['apply'] = self.output.stdout
            os.remove(self.planfile)
        elif action == 'force_unlock' and 'successfully unlocked' in self.output.stdout:
            del self.stats[self.env][self.stage][self.cap]['locked']

    def run_action(self, action):
        subprocess.run(['git', 'checkout', self.tag], capture_output=True, cwd=self.module_repo_folder, check=False, text=True)
        
        self.output = subprocess.run(self.generate_cmd(action), cwd=self.path, capture_output=True, check=False, text=True)
        
        subprocess.run(['git', 'checkout', 'development'], capture_output=True, cwd=self.module_repo_folder, check=False, text=True)

        self.update_stats(action)
        
    def do_plan(self, line):
        args = self.validate_input('plan', line)

        if args:
            envs = args[Arg.ENVIRONMENT.value]
            caps = args[Arg.CAPABILITY.value]
            
            for env in tqdm(envs):
                for cap in tqdm(caps):
                    tqdm.write(f'Planing: {env} -> {cap}')
                    
                    self.trigger_aws_auth()

                    self.stage = args[Arg.STAGE.value]
                    self.env = env
                    self.cap = cap

                    self.run_action('plan')

                    if 'Error' not in self.output.stderr:
                        self.run_action('show')

    def do_apply(self, line):
        args = self.validate_input('apply', line)
        
        if args:
            envs = args[Arg.ENVIRONMENT.value]
            caps = args[Arg.CAPABILITY.value]

            for env in tqdm(envs):
                for cap in tqdm(caps):
                    tqdm.write(f'Applying: {env} -> {cap}')

                    self.trigger_aws_auth()

                    self.stage = args[Arg.STAGE.value]
                    self.env = env
                    self.cap = cap
                    
                    if not os.path.exists(self.planfile):
                        self.do_plan(line)

                    if os.path.exists(self.planfile):
                        self.run_action('apply')
                    
    def do_destroy(self, line):
        self.trigger_aws_auth()
        
        args = self.validate_input('destroy', line)
        print('Not implemented yet')
    
    def do_force_unlock(self, line):
        for env in tqdm(self.stats):
            for stage in tqdm(self.stats[env]):
                for cap, info in self.stats[env][stage].items():
                    self.trigger_aws_auth()

                    if info.get('locked'):
                        self.env = env
                        self.stage = stage
                        self.cap = cap
                        self.locked_id = info['locked']

                        self.run_action('force_unlock')

    def do_print(self, line):
        if len(line.split(' ')) == 1:
            self.print_formated_stats()
        else:
            args = self.validate_input('print', line)

            if args:
                envs = args[Arg.ENVIRONMENT.value]
                caps = args[Arg.CAPABILITY.value]

                for env in envs:
                    for cap in caps:
                        self.stage = args[Arg.STAGE.value]
                        self.env = env
                        self.cap = cap
                        
                        print(self.stats[self.env][self.stage][self.cap].get(args[Arg.PRINTC.value], f'No {args[Arg.PRINTC.value].capitalize()}'))

    def do_update(self, line):
        pass
        
    def do_bye(self, line):
        print('Thank you for using terractl')
        return True
    
    def complete_print(self, text, line, begidx, endidx):
        arg_number = len(line.split(' ')[1:]) - 1

        if text:
            if arg_number == Arg.ENVIRONMENT.value:
                return [env for env in self.stats if env.startswith(text)]
            elif arg_number == Arg.STAGE.value:
                return [stage for env in self.stats for stage in self.stats[env] if stage.startswith(text)]
            elif arg_number == Arg.CAPABILITY.value:
                return [cap  for env in self.stats for stage in self.stats[env] for cap in self.stats[env][stage] if cap.startswith(text)]
            elif arg_number == Arg.PRINTC.value:
                return [printc for printc in self.print_commands if printc.startswith(text)]
        return []
    
    def completedefault(self, text, line, start_index, end_index):
        arg_number = len(line.split(' ')[1:]) - 1
        
        if text and self.autocomplete_lists.get(arg_number):
            if arg_number in [Arg.ENVIRONMENT.value, Arg.CAPABILITY.value]:
                text = text.split(',')[-1]
            return [
                elem for elem in self.autocomplete_lists.get(arg_number)
                if elem.startswith(text)
            ]
        
        return self.autocomplete_lists.get(arg_number)

    def precmd(self, line):
        line = line.lower()
        return line

if __name__ == '__main__':
    terractl = Terractl()
    terractl.cmdloop()
